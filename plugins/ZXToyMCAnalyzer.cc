// -*- C++ -*-
//
// Package:    UserCode/ZXToyMCAnalyzer
// Class:      ZXToyMCAnalyzer
//
/**\class ZXToyMCAnalyzer ZXToyMCAnalyzer.cc UserCode/ZXToyMCAnalyzer/plugins/ZXToyMCAnalyzer.cc

 Description: pp to ZX toy MC analyzer

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  Pedro Vieira De Castro Ferreira Da Silva
//         Created:  Thu, 17 Jan 2019 12:17:29 GMT
//
//


// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "DataFormats/ProtonReco/interface/ForwardProton.h"
#include "DataFormats/CTPPSReco/interface/CTPPSLocalTrackLite.h"
#include "DataFormats/CTPPSDetId/interface/CTPPSDetId.h"
#include "SimDataFormats/GeneratorProducts/interface/HepMCProduct.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/InputTag.h"

#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"

#include "TTree.h"
#include "TString.h"
#include "TLorentzVector.h"

#include <map>
#include <vector>
#include <algorithm>

//
// class declaration
//
bool sortByPt (TLorentzVector i,TLorentzVector j) { return (i.Pt()>j.Pt()); }
class ZXToyMCAnalyzer : public edm::one::EDAnalyzer<edm::one::SharedResources>  {
   public:
  explicit ZXToyMCAnalyzer(const edm::ParameterSet&);
  ~ZXToyMCAnalyzer();
  
  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);
  
  
private:
  virtual void beginJob() override;
  virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
  virtual void endJob() override;
  
  // ----------member data ---------------------------
  edm::EDGetTokenT<edm::HepMCProduct> genToken_;
  std::vector<edm::EDGetTokenT<std::vector<reco::ForwardProton>>> tokenRecoProtons_;
  edm::Service<TFileService> fs;
  TH1F *countH_,*genpH_,*genmH_,*genpzppH_,*recpH_,*recmH_,*recpzppH_,*mprecpH_,*mprecmH_,*mprecpzppH_;
  TTree *tree_;
  int evcat_;
  Bool_t isSS_,isSF_,isZ_,isA_,isRPFiducial_;
  Float_t evwgt_,nvtx_,nch_;
  Float_t pzpp_;
  Int_t beamXangle_;
  Float_t l1pt_,l1eta_,l1phi_,l1m_;
  Float_t l2pt_,l2eta_,l2phi_,l2m_;
  Float_t bpt_,beta_,bphi_,bm_;

  int nProtons_;
  Bool_t isFarRPProton_[50], isMultiRPProton_[50], isPosRPProton_[50];
  Int_t protonRPid_[50];
  Float_t protonX_[50],protonY_[50],protonCsi_[50],genProtonCsi_[50];
};

//
// constructors and destructor
//
ZXToyMCAnalyzer::ZXToyMCAnalyzer(const edm::ParameterSet& iConfig)
 :
  genToken_(consumes<edm::HepMCProduct>(edm::InputTag("generator:unsmeared"))),
  beamXangle_(iConfig.getUntrackedParameter<int>("beamXangle"))
{
  
  tokenRecoProtons_.push_back(consumes<std::vector<reco::ForwardProton>>(edm::InputTag("ctppsProtons:singleRP")));
  tokenRecoProtons_.push_back(consumes<std::vector<reco::ForwardProton>>(edm::InputTag("ctppsProtons:multiRP")));

  //now do what ever initialization is needed
  countH_=fs->make<TH1F>("count",";count;",2,0,2);
  genpH_=fs->make<TH1F>("gencsip",";#xi;Events",50,0,0.18);
  genmH_=fs->make<TH1F>("gencsim",";#xi;Events",50,0,0.18);
  genpzppH_=fs->make<TH1F>("genpzpp",";Generated p_{z}(pp) [GeV];Events",100,-2000,2000);
  recpH_=fs->make<TH1F>("reccsip",";#xi;Events",50,0,0.18);
  recmH_=fs->make<TH1F>("reccsim",";#xi;Events",50,0,0.18);
  recpzppH_=fs->make<TH1F>("recpzpp",";Generated p_{z}(pp) [GeV];Events",100,-2000,2000);
  mprecpH_=fs->make<TH1F>("mpreccsip",";#xi;Events",50,0,0.18);
  mprecmH_=fs->make<TH1F>("mpreccsim",";#xi;Events",50,0,0.18);
  mprecpzppH_=fs->make<TH1F>("mprecpzpp",";Generated p_{z}(pp) [GeV];Events",100,-2000,2000);
  
  tree_=fs->make<TTree>("data","data");
  tree_->Branch("evcat",&evcat_,"evcat/I");
  tree_->Branch("isSS",&isSS_,"isSS/O");
  tree_->Branch("isSF",&isSF_,"isSF/O");
  tree_->Branch("isZ",&isZ_,"isZ/O");
  tree_->Branch("isA",&isA_,"isA/O");
  tree_->Branch("isRPFiducial",&isRPFiducial_,"isRPFiducial/O");
  tree_->Branch("gen_pzpp",&pzpp_,"gen_pzpp/F");
  tree_->Branch("evwgt",&evwgt_,"evwgt/F");
  tree_->Branch("nvtx",&nvtx_,"nvtx/F"); nvtx_=0;
  tree_->Branch("nch",&nch_,"nch/F"); nch_=0;
  tree_->Branch("beamXangle",&beamXangle_,"beamXangle/I");
  tree_->Branch("l1pt",&l1pt_,"l1pt/F");
  tree_->Branch("l1eta",&l1eta_,"l1eta/F");
  tree_->Branch("l1phi",&l1phi_,"l1phi/F");
  tree_->Branch("ml1",&l1m_,"ml1/F");
  tree_->Branch("l2pt",&l2pt_,"l2pt/F");
  tree_->Branch("l2eta",&l2eta_,"l2eta/F");
  tree_->Branch("l2phi",&l2phi_,"l2phi/F");
  tree_->Branch("ml2",&l2m_,"ml2/F");
  tree_->Branch("bosonpt",&bpt_,"bosonpt/F");
  tree_->Branch("bosoneta",&beta_,"bosoneta/F");
  tree_->Branch("bosonphi",&bphi_,"bosonphi/F");
  tree_->Branch("mboson",&bm_,"mboson/F");
  tree_->Branch("nProtons",       &nProtons_,        "nProtons/I");
  tree_->Branch("isFarRPProton",   isFarRPProton_,   "isFarRPProton[nProtons]/O");
  tree_->Branch("isPosRPProton",   isPosRPProton_,   "isPosRPProton[nProtons]/O");
  tree_->Branch("isMultiRPProton", isMultiRPProton_, "isMultiRPProton[nProtons]/O");
  tree_->Branch("protonRPid",      protonRPid_,      "protonRPid[nProtons]/I");
  tree_->Branch("protonCsi",       protonCsi_,       "protonCsi[nProtons]/F");
  tree_->Branch("protonX",         protonX_,         "protonX[nProtons]/F");
  tree_->Branch("protonY",         protonY_,         "protonY[nProtons]/F");
  tree_->Branch("genProtonCsi",    genProtonCsi_,    "genProtonCsi[nProtons]/F");
}


ZXToyMCAnalyzer::~ZXToyMCAnalyzer()
{

   // do anything here that needs to be done at desctruction time
   // (e.g. close files, deallocate resources etc.)

}


//
// member functions
//

// ------------ method called for each event  ------------
void
ZXToyMCAnalyzer::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
   using namespace edm;
   using namespace std;

   countH_->Fill(0.);
   evwgt_=1.0;

   //get leptons and protons
   edm::Handle<edm::HepMCProduct> gen;
   iEvent.getByToken(genToken_, gen);
   auto evt = gen->GetEvent();
   std::vector<TLorentzVector> protons,leptons;
   TLorentzVector boson;
   std::vector<int> lid;
   for (auto it_vtx = evt->vertices_begin(); it_vtx != evt->vertices_end(); ++it_vtx)
     {
       auto vtx = *it_vtx;
       
       // loop over outgoing particles
       for (auto it_part = vtx->particles_out_const_begin(); it_part != vtx->particles_out_const_end(); ++it_part)
         {
           auto part = *it_part;
           
           int id(part->pdg_id());           
           if(id==2212) {
             protons.push_back(TLorentzVector(part->momentum().x(), part->momentum().y(), part->momentum().z(), part->momentum().t()));
           }
           else if(abs(id)==11 || abs(id)==13) {
             lid.push_back(id);
             leptons.push_back(TLorentzVector(part->momentum().x(), part->momentum().y(), part->momentum().z(), part->momentum().t()));
           } else if(abs(id)==23) {
             boson=TLorentzVector(part->momentum().x(), part->momentum().y(), part->momentum().z(), part->momentum().t());
           }
         }
     }   
   if(lid.size()<2) return;

   //boson kinematics
   //TLorentzVector boson(leptons[0]+leptons[1]);
   bpt_=boson.Pt();
   beta_=boson.Eta();
   bphi_=boson.Phi();
   bm_=boson.M();
   
   //if the mass of the boson is 0, this is a photon emulation
   if(bm_>20) {
       sort(leptons.begin(),leptons.end(),sortByPt);
       if(leptons[0].Pt()<30 || leptons[1].Pt()<20) return;
       if(fabs(leptons[0].Eta())>2.5 || fabs(leptons[1].Eta())>2.5) return;   
       if(bm_<20) return;
       isZ_=(isSF_ && !isSS_ && fabs(bm_-91)<10);
       isA_=false;
       evcat_=abs(lid[0]*lid[1]);
       isSS_=lid[0]*lid[1]>0 ? true : false;
       isSF_=(abs(lid[0])==abs(lid[1])? true: false);
       
   } else {
     leptons[0]=TLorentzVector(0,0,0,0);
     leptons[1]=TLorentzVector(0,0,0,0);
     bm_=0.;
     if(bpt_<95) return;
     if(fabs(beta_)>1.4442) return;
     isZ_=false;
     isA_=true;
     evcat_=22;
     isSS_=false;
     isSF_=false;
   }

   //lepton kinematics
   l1pt_=leptons[0].Pt();
   l1eta_=leptons[0].Eta();
   l1phi_=leptons[0].Phi();   
   l1m_=leptons[0].M();
   l2pt_=leptons[1].Pt();
   l2eta_=leptons[1].Eta();
   l2phi_=leptons[1].Phi();   
   l2m_=leptons[1].M();
   
   //true momentum loss
   float xip( protons[0].Pz()>0 ? 1. - protons[0].E() / 6500. : 1. - protons[1].E() / 6500. );
   float xim( protons[0].Pz()>0 ? 1. - protons[1].E() / 6500. : 1. - protons[0].E() / 6500. );
   pzpp_=(protons[0]+protons[1]).Pz();
   if(protons[1].Pz()>0) pzpp_ *= -1;
   genpH_->Fill(xip);
   genmH_->Fill(xim);
   genpzppH_->Fill(pzpp_);

   isRPFiducial_ = (pzpp_>-500 && pzpp_<700);

   //RP tracks
   nProtons_=0;
   int nsingleRP(0),nmultiRP(0);
   vector<std::pair<int,float> > nearCsis;
   for(size_t ip=0; ip<tokenRecoProtons_.size(); ip++) {
     edm::Handle<std::vector<reco::ForwardProton> > recoProtons;
     iEvent.getByToken(tokenRecoProtons_[ip], recoProtons);
     for (const auto & proton : *recoProtons)
       {
         unsigned int method=(int) proton.method();

         //do this only for pixels/strips (otherwise there is a feat in multiRP efficiency...)
         if(method==0 && !proton.validFit()) continue;

         const CTPPSLocalTrackLiteRefVector &trks=proton.contributingLocalTracks();       
         CTPPSDetId detid( (*(trks.begin()))->getRPId() );
         unsigned int arm=detid.arm(); // 0 = sector 4-5 ; 1 = sector 5-6
         unsigned int sta=detid.station(); // 0 = near pot ; 2 = far pot
         unsigned int pot=detid.rp(); 
         const unsigned short pot_raw_id = 100*arm+10*sta+pot;
         
         protonRPid_[nProtons_]      = pot_raw_id;
         isFarRPProton_[nProtons_]   = (pot_raw_id==123 || pot_raw_id==23);
         isMultiRPProton_[nProtons_] = (method==1);
         isPosRPProton_[nProtons_]   = (pot_raw_id<100);
         protonCsi_[nProtons_]       = proton.xi();
         genProtonCsi_[nProtons_]    = (pot_raw_id<100 ? xip : xim);
         protonX_[nProtons_]         = (*(trks.begin()))->getX();
         protonY_[nProtons_]         = (*(trks.begin()))->getY();

         //special procedure for multiRP
         //if first track was not a pixel (far detector), try to find it in the track list
         if(method==1 && !isFarRPProton_[nProtons_]) {
           for(const auto &t:trks) {
             CTPPSDetId detid( t->getRPId() );
             unsigned int arm=detid.arm(); // 0 = sector 4-5 ; 1 = sector 5-6     
             unsigned int sta=detid.station(); // 0 = near pot ; 2 = far pot
             unsigned int pot=detid.rp();
             const unsigned short pot_raw_id = 100*arm+10*sta+pot;
             if(pot_raw_id!=123 && pot_raw_id!=23) continue;
             isFarRPProton_[nProtons_]=true;
             isPosRPProton_[nProtons_]   = (pot_raw_id<100);
             protonX_[nProtons_]         = t->getX();
             protonY_[nProtons_]         = t->getY();
           }
         }

         if(method==0) nsingleRP++;
         if(method==1) nmultiRP++;
         
         if(isPosRPProton_[nProtons_]){
           if(method==0 && isFarRPProton_[nProtons_])  recpH_->Fill(xip);
           else                                        mprecpH_->Fill(xip);
         }else{
           recmH_->Fill(xim);
           if(method==0 && isFarRPProton_[nProtons_]) recmH_->Fill(xim);
           else                                       mprecmH_->Fill(xim);
         }
           
         nProtons_++;
       }     
   }
   

   //for the efficiency
   if(nsingleRP==2) recpzppH_->Fill(pzpp_);
   if(nmultiRP==2)  mprecpzppH_->Fill(pzpp_);

   countH_->Fill(1.);
   tree_->Fill();
}


// ------------ method called once each job just before starting event loop  ------------
void
ZXToyMCAnalyzer::beginJob()
{
}

// ------------ method called once each job just after ending the event loop  ------------
void
ZXToyMCAnalyzer::endJob()
{
}

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void
ZXToyMCAnalyzer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(ZXToyMCAnalyzer);
