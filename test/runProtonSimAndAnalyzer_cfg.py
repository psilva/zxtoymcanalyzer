import FWCore.ParameterSet.Config as cms

from FWCore.ParameterSet.VarParsing import VarParsing
options = VarParsing ('python')
options.register('fileList',
                 'data/sdposz_zmm.txt',
                 VarParsing.multiplicity.singleton,
                 VarParsing.varType.string,
                 "list of files to process"
                 )
options.register('beamXangle',
                 120,
                 VarParsing.multiplicity.singleton,
                 VarParsing.varType.int,
                 "set this value as beam xangle"
                 )
options.parseArguments()

process = cms.Process("Analysis")

from Validation.CTPPS.simu_config.year_2017_postTS2_cff import *
process = cms.Process('CTPPSDirectSimulation', era)

process.load("Validation.CTPPS.simu_config.year_2017_postTS2_cff")
UseCrossingAngle(options.beamXangle, process)

# minimal logger settings
process.MessageLogger = cms.Service("MessageLogger",
    statistics = cms.untracked.vstring(),
    destinations = cms.untracked.vstring('cout'),
    cout = cms.untracked.PSet(
        threshold = cms.untracked.string('WARNING')
    )
)

# number of events
process.maxEvents = cms.untracked.PSet(
  input = cms.untracked.int32(-1)
)

#define the source
fileList=[line. rstrip('\n') for line in open(options.fileList)]
process.source = cms.Source("PoolSource",
                            fileNames = cms.untracked.vstring(fileList),
                            duplicateCheckMode = cms.untracked.string('noDuplicateCheck') 
                            )


# distribution plotter
process.ctppsTrackDistributionPlotter = cms.EDAnalyzer("CTPPSTrackDistributionPlotter",
  tagTracks = cms.InputTag("ctppsLocalTrackLiteProducer"),
  outputFile = cms.string("output_shape_smear.root")
)

# acceptance plotter
process.ctppsAcceptancePlotter = cms.EDAnalyzer("CTPPSAcceptancePlotter",
  tagHepMC = cms.InputTag("generator","unmeared"),
  tagTracks = cms.InputTag("ctppsLocalTrackLiteProducer"),

  rpId_45_F = process.rpIds.rp_45_F,
  rpId_45_N = process.rpIds.rp_45_N,
  rpId_56_N = process.rpIds.rp_56_N,
  rpId_56_F = process.rpIds.rp_56_F,

  outputFile = cms.string("acceptance.root")
)

# generator plots
process.load("SimCTPPS.Generators.PPXZGeneratorValidation_cfi")
process.ppxzGeneratorValidation.tagHepMC = cms.InputTag("generator", "unsmeared")
process.ppxzGeneratorValidation.tagRecoTracks = cms.InputTag("ctppsLocalTrackLiteProducer")
process.ppxzGeneratorValidation.tagRecoProtonsSingleRP = cms.InputTag("ctppsProtons", "singleRP")
process.ppxzGeneratorValidation.tagRecoProtonsMultiRP = cms.InputTag("ctppsProtons", "multiRP")
process.ppxzGeneratorValidation.referenceRPDecId_45 = process.rpIds.rp_45_F
process.ppxzGeneratorValidation.referenceRPDecId_56 = process.rpIds.rp_56_F
process.ppxzGeneratorValidation.outputFile = "ppxzGeneratorValidation.root"

# processing path
process.p = cms.Path(
  process.generator
  * process.beamDivergenceVtxGenerator
  * process.ctppsDirectProtonSimulation

  * process.reco_local
  * process.ctppsProtons

  * process.ctppsTrackDistributionPlotter
  * process.ctppsAcceptancePlotter
  * process.ppxzGeneratorValidation
)


# output configuration
process.output = cms.OutputModule("PoolOutputModule",
  fileName = cms.untracked.string("ntuple.root"),
  splitLevel = cms.untracked.int32(0),
  eventAutoFlushCompressedSize=cms.untracked.int32(-900),
  compressionAlgorithm=cms.untracked.string("LZMA"),
  compressionLevel=cms.untracked.int32(4),
  outputCommands = cms.untracked.vstring(
    'drop *',
    'keep edmHepMCProduct_*_*_*',
    'keep CTPPSLocalTrackLites_*_*_*',
    'keep recoForwardProtons_*_*_*'
  )
)

process.outpath = cms.EndPath(process.output)
