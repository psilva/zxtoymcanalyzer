#!/bin/bash

cmssw=${1}
inF=${2}
xangle=${3}
tag=${4}
output=${5}

cd ${cmssw}/src
eval `scram r -sh`
cd - 
cmsRun ${cmssw}/src/UserCode/ZXToyMCAnalyzer/test/runZXToyMCAnalyzer_cfg.py inputFile=${inF} beamXangle=${xangle} maxEvents=-1 outputFile=ntuple.root;
xrdcp -f ntuple.root root://eoscms//${output}/${tag}.root
rm ntuple.root
