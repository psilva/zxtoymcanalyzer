import ROOT
import numpy as np
import sys
from array import array

url='/eos/cms/store/cmst3/group/top/RunIIReReco/2017/vxsimulations_28Jan'
bname=sys.argv[1]
if len(sys.argv)>2: 
    url=sys.argv[2]

ZXXSECS = {120:0.269,130:0.273,140:0.143,150:0.293}
AXXSECS = {120:0.372,130:0.295,140:0.162,150:0.171}

fout = ROOT.TFile("%sX_validation.root"%bname, "RECREATE")
fout.cd()
varnames=['mX','xangle','postTS2','wgt','gen_pzpp']
for im in ['gen','multi','pix','strip']:
    varnames += [ '%s_%s'%(x,im) for x in ['csiNeg','csiPos','mpp','mmass'] ]
varvals=[0.]*len(varnames)
output_tuple = ROOT.TNtuple("tuple","tuple",":".join(varnames))

for m in [600,800,1000,1200,1400,1600]: 
    for a in [120,130,140,150]:
        
        awgt=ZXXSECS[a] if bname=='Z' else AXXSECS[a]

        for period,pwgt in [('pre',0.3512),
                            ('post',0.6488)]:

            print 'M=',m,'angle=',a,period,'weight=',pwgt

            varvals[0]=m
            varvals[1]=a
            varvals[2]=0 if period=='pre' else 1

            #read proton data
            fIn=ROOT.TFile.Open('{0}/{1}_m_X_{2}_xangle_{3}_2017_{4}TS2.root'.format(url,bname,m,a,period))
            data=fIn.Get('analysis/data')
            nentries=data.GetEntries()

            nSignalWgtSum=0
            for i in xrange(0,nentries):
                data.GetEntry(i)
                nSignalWgtSum += ROOT.TMath.Gaus(data.gen_pzpp,0,0.391*m+624.)

            for i in xrange(nentries):
                data.GetEntry(i)
                
                varvals[3]=(awgt*pwgt*ROOT.TMath.Gaus(data.gen_pzpp,0,0.391*m+624.))/(nSignalWgtSum*nentries)
                varvals[4]=data.gen_pzpp
                
                #boson kinematics
                boson=ROOT.TLorentzVector(0,0,0,0)
                boson.SetPtEtaPhiM(data.bosonpt,data.bosoneta,data.bosonphi,data.mboson)
                
                #proton candidates
                protons=[[None]*4,[None]*4]
                for itk in range(data.nProtons):
                    isMulti = data.isMultiRPProton[itk]
                    isFar   = data.isFarRPProton[itk]
                    isPos   = data.isPosRPProton[itk]
                    protons[isPos][0]=data.genProtonCsi[itk]
                    if isFar:
                        idx=2
                    else:
                        idx=3
                    if isMulti:
                        idx=1
                    protons[isPos][idx]=data.protonCsi[itk]
                
                #diproton candidates
                for im in range(4):
                    csiNeg=protons[0][im]
                    csiPos=protons[1][im]

                    ioff=im*4
                    varvals[5+ioff]=csiNeg if csiNeg else -1
                    varvals[6+ioff]=csiPos if csiPos else -1
                    varvals[7+ioff]=-1
                    varvals[8+ioff]=-1

                    if csiNeg and csiPos:
                        beamP=0.5*13000.
                        pp=ROOT.TLorentzVector(0.,0.,beamP*(csiPos-csiNeg),beamP*(csiPos+csiNeg))
                        mmass=pp-boson
                        varvals[7+ioff]=pp.M()
                        varvals[8+ioff]=mmass.M()

                output_tuple.Fill( array("f",varvals) )

# write the tuple to the output file and close it
fout.cd()
output_tuple.Write()
