#!/bin/bash

condor="condor_ntuplizer.sub"
echo "executable  = ${CMSSW_BASE}/src/UserCode/ZXToyMCAnalyzer/test/wrapProcessing.sh" > $condor
echo "output      = ${condor}.out" >> $condor
echo "error       = ${condor}.err" >> $condor
echo "log         = ${condor}.log" >> $condor
echo "+JobFlavour =\"workday\"">> $condor

#samples="Z_m_X_600_xangle_120_2017_postTS2.root,Z_m_X_600_xangle_120_2017_postTS2_opt_v1_simu_reco.root,Z_m_X_600_xangle_120_2017_preTS2.root,Z_m_X_600_xangle_120_2017_preTS2_opt_v1_simu_reco.root,Z_m_X_600_xangle_130_2017_postTS2.root,Z_m_X_600_xangle_130_2017_postTS2_opt_v1_simu_reco.root,Z_m_X_600_xangle_130_2017_preTS2.root,Z_m_X_600_xangle_130_2017_preTS2_opt_v1_simu_reco.root,Z_m_X_600_xangle_140_2017_postTS2_opt_v1_simu_reco.root,Z_m_X_600_xangle_140_2017_preTS2.root,Z_m_X_600_xangle_150_2017_postTS2.root,Z_m_X_600_xangle_150_2017_postTS2_opt_v1_simu_reco.root,Z_m_X_600_xangle_150_2017_preTS2_opt_v1_simu_reco.root,Z_m_X_660_xangle_120_2017_postTS2.root,Z_m_X_660_xangle_120_2017_postTS2_opt_v1_simu_reco.root,Z_m_X_660_xangle_120_2017_preTS2.root,Z_m_X_660_xangle_120_2017_preTS2_opt_v1_simu_reco.root,Z_m_X_660_xangle_130_2017_postTS2.root,Z_m_X_660_xangle_130_2017_postTS2_opt_v1_simu_reco.root,Z_m_X_660_xangle_130_2017_preTS2.root,Z_m_X_660_xangle_140_2017_postTS2.root,Z_m_X_660_xangle_140_2017_postTS2_opt_v1_simu_reco.root,Z_m_X_660_xangle_140_2017_preTS2.root,Z_m_X_660_xangle_140_2017_preTS2_opt_v1_simu_reco.root,Z_m_X_660_xangle_150_2017_postTS2.root,Z_m_X_660_xangle_150_2017_postTS2_opt_v1_simu_reco.root,Z_m_X_660_xangle_150_2017_preTS2.root,Z_m_X_660_xangle_150_2017_preTS2_opt_v1_simu_reco.root,Z_m_X_720_xangle_120_2017_postTS2.root,Z_m_X_720_xangle_120_2017_postTS2_opt_v1_simu_reco.root,Z_m_X_720_xangle_120_2017_preTS2.root,Z_m_X_720_xangle_130_2017_postTS2.root,Z_m_X_720_xangle_130_2017_postTS2_opt_v1_simu_reco.root,Z_m_X_720_xangle_130_2017_preTS2.root,Z_m_X_720_xangle_130_2017_preTS2_opt_v1_simu_reco.root,Z_m_X_720_xangle_140_2017_postTS2.root,Z_m_X_720_xangle_140_2017_postTS2_opt_v1_simu_reco.root,Z_m_X_720_xangle_140_2017_preTS2.root,Z_m_X_720_xangle_140_2017_preTS2_opt_v1_simu_reco.root,Z_m_X_720_xangle_150_2017_postTS2.root,Z_m_X_720_xangle_150_2017_preTS2.root,Z_m_X_720_xangle_150_2017_preTS2_opt_v1_simu_reco.root,gamma_m_X_600_xangle_120_2017_postTS2.root,gamma_m_X_600_xangle_120_2017_postTS2_opt_v1_simu_reco.root,gamma_m_X_600_xangle_120_2017_preTS2_opt_v1_simu_reco.root,gamma_m_X_600_xangle_130_2017_postTS2.root,gamma_m_X_600_xangle_130_2017_postTS2_opt_v1_simu_reco.root,gamma_m_X_600_xangle_130_2017_preTS2.root,gamma_m_X_600_xangle_130_2017_preTS2_opt_v1_simu_reco.root,gamma_m_X_600_xangle_140_2017_postTS2.root,gamma_m_X_600_xangle_140_2017_postTS2_opt_v1_simu_reco.root,gamma_m_X_600_xangle_140_2017_preTS2_opt_v1_simu_reco.root,gamma_m_X_600_xangle_150_2017_postTS2.root,gamma_m_X_600_xangle_150_2017_postTS2_opt_v1_simu_reco.root,gamma_m_X_600_xangle_150_2017_preTS2.root,gamma_m_X_600_xangle_150_2017_preTS2_opt_v1_simu_reco.root,gamma_m_X_660_xangle_120_2017_postTS2.root,gamma_m_X_660_xangle_120_2017_preTS2.root,gamma_m_X_660_xangle_120_2017_preTS2_opt_v1_simu_reco.root,gamma_m_X_660_xangle_130_2017_postTS2.root,gamma_m_X_660_xangle_130_2017_postTS2_opt_v1_simu_reco.root,gamma_m_X_660_xangle_130_2017_preTS2.root,gamma_m_X_660_xangle_130_2017_preTS2_opt_v1_simu_reco.root,gamma_m_X_660_xangle_140_2017_postTS2.root,gamma_m_X_660_xangle_140_2017_postTS2_opt_v1_simu_reco.root,gamma_m_X_660_xangle_140_2017_preTS2.root,gamma_m_X_660_xangle_140_2017_preTS2_opt_v1_simu_reco.root,gamma_m_X_660_xangle_150_2017_postTS2.root,gamma_m_X_660_xangle_150_2017_postTS2_opt_v1_simu_reco.root,gamma_m_X_660_xangle_150_2017_preTS2.root,gamma_m_X_660_xangle_150_2017_preTS2_opt_v1_simu_reco.root,gamma_m_X_720_xangle_120_2017_postTS2.root,gamma_m_X_720_xangle_120_2017_postTS2_opt_v1_simu_reco.root,gamma_m_X_720_xangle_120_2017_preTS2.root,gamma_m_X_720_xangle_120_2017_preTS2_opt_v1_simu_reco.root,gamma_m_X_720_xangle_130_2017_postTS2_opt_v1_simu_reco.root,gamma_m_X_720_xangle_130_2017_preTS2.root,gamma_m_X_720_xangle_130_2017_preTS2_opt_v1_simu_reco.root,gamma_m_X_720_xangle_140_2017_postTS2_opt_v1_simu_reco.root,gamma_m_X_720_xangle_140_2017_preTS2.root,gamma_m_X_720_xangle_140_2017_preTS2_opt_v1_simu_reco.root,gamma_m_X_720_xangle_150_2017_postTS2.root,gamma_m_X_720_xangle_150_2017_postTS2_opt_v1_simu_reco.root,gamma_m_X_720_xangle_150_2017_preTS2.root,gamma_m_X_720_xangle_150_2017_preTS2_opt_v1_simu_reco.root"

indir=$1
outdir=$2
mkdir -p /eos/cms/${2}
a=(`find  ${indir}  -name "ntuple*"`)
for i in ${a[@]}; do
    tag=${i/${indir}/}
    tag=`dirname ${tag}`
    tag=${tag//\//_}
    tag="${tag:1}"

    #use to filter specific samples
    #if [[ ! $samples == *"${tag}"* ]]; then
    #    continue
    #fi

    xangle=120
    if [[ $tag == *"xangle_130"* ]]; then
        xangle=130;
    fi
    if [[ $tag == *"xangle_140"* ]]; then
        xangle=140;
    fi
    if [[ $tag == *"xangle_150"* ]]; then
        xangle=150;
    fi
    
    
    echo "arguments   = ${CMSSW_BASE} ${i} ${xangle} ${tag} ${outdir}" >> $condor
    echo "queue 1" >> $condor
    
    #run locally
    #cmsRun test/runZXToyMCAnalyzer_cfg.py inputFile=${i} beamXangle=${xangle} maxEvents=-1 outputFile=ntuple.root;
    #    mv -v ntuple.root ${outdir}/${tag}.root;    
done


echo "Submitting $condor"
condor_submit $condor
