import FWCore.ParameterSet.Config as cms

from FWCore.ParameterSet.VarParsing import VarParsing
options = VarParsing ('python')
options.register('inputFile', 
                 '/afs/cern.ch/user/j/jkaspar/public/ctpps/ppxz/m_X_1000/ntuple.root',
                 VarParsing.multiplicity.singleton,
                 VarParsing.varType.string,
                 "input file to process"
                 )
options.register('beamXangle',
                 0,
                 VarParsing.multiplicity.singleton,
                 VarParsing.varType.int,
                 "set this value as beam xangle"
                 )
options.parseArguments()

process = cms.Process("Analysis")

#message logger
process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr.threshold = ''
process.MessageLogger.cerr.FwkReport.reportEvery = 1000


# set input to process
process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(options.maxEvents) )
process.source = cms.Source("PoolSource",
                            fileNames = cms.untracked.vstring('file:'+options.inputFile),
                            duplicateCheckMode = cms.untracked.string('noDuplicateCheck') 
                            )

process.TFileService = cms.Service("TFileService",
                                   fileName = cms.string(options.outputFile),
                                   closeFileFast = cms.untracked.bool(True)
                                   )

process.load('SimGeneral.HepPDTESSource.pythiapdt_cfi')
process.analysis = cms.EDAnalyzer("ZXToyMCAnalyzer",
                                  beamXangle=cms.untracked.int32(options.beamXangle))

process.seq=cms.Sequence(process.analysis)

process.p = cms.Path(process.seq)
