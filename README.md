# ZXToyMCAnalyzer

pp to ZX toy MC analyzer

Installation (use SLC7)

```
cmsrel CMSSW_10_6_0
cd CMSSW_10_6_0/src
cmsenv
git clone https://gitlab.cern.ch/psilva/zxtoymcanalyzer.git UserCode/ZXToyMCAnalyzer
scram b
```

Running the ntuplizer

Running over a single file
```
cmsRun test/runZXToyMCAnalyzer_cfg.py inputFile=edm_input.root outputFile=ntuple.root maxEvents=-1 beamXangle=120
```

Process all in sub-directories and copying it to an output directory (delegates to condor by default)

```
simDir=/eos/cms/store/cmst3/group/top/RunIIReReco/2017/vxsimulations_19May
sh test/processAll.sh  /eos/cms/store/group/phys_pps/ppxz/version10 ${simDir}
```

Prepare a small ntuple for validation

```
for x in Z gamma; do
    python test/fillValidationNtuple.py ${x} ${simDir} &
done
```

Can be analyzed with the python notebook in swan